const presets = [
    ["@babel/preset-env", {
        "targets":{
            "node": "6.10"
        }
    }]
]

module.exports = {presets};