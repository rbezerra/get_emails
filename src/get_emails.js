export const get_emails = (user_id, connection, db_func) => {
    return new Promise((resolve, reject) => {
        if (!user_id) {
            reject(new Error('Code : [90] You should pass a user id as a parameter'));
        }

        if (!connection || typeof connection !== 'object') {
            reject(new Error('Code : [90] You should pass a valid connection as parameter'));
        }

        if (!db_func || typeof dbfunc !== 'function') {
            reject(new Error('Code : [90] You should pass a database function'));
        }

        const query = `
            SELECT 
                id_em_emails,
                email,
                verified,
                preferred
            FROM 
                em_emails
            WHERE
                id_em_users = ${user_id}
        `;

        db_func(connection, query)
            .then((emails) => {
                const res = {
                    code: 200,
                    emails: emails
                };

                resolve(res);
            })
            .catch((err) => reject(err));
    });
};

