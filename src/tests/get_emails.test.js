
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { get_emails } from '../get_emails';

chai.use(chaiAsPromised);

const expect = chai.expect;

const mock_obj = {
    host: "host",
    database: "database",
    user: "user",
    password: "password"
};

const mock_function = (connection, query) => {
    return new Promise((resolve, reject) => resolve([
        {
            userId: 1,
            email: {
                to: "to@gmail.com",
                from: "from@gmail.com",
                message: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut facilisis sem at felis elementum placerat. Nunc dapibus iaculis luctus. Aenean et volutpat augue. Aenean vehicula ultrices orci ut porta. Praesent metus dui, dapibus vestibulum aliquet et, interdum ut eros. Donec volutpat leo diam, vitae elementum magna ullamcorper at. Suspendisse ut molestie urna. Etiam bibendum vel leo nec elementum. Fusce ut justo in nibh vehicula euismod. Ut cursus finibus mi, at ultricies augue porttitor nec. Aenean in mi eu turpis porttitor dignissim eu eu lorem. Vivamus vitae maximus libero. Quisque scelerisque tincidunt tempus. Quisque sollicitudin pharetra consectetur. Mauris a sapien dapibus, bibendum arcu eu, malesuada libero.`
            }
        },
        {
            userId: 1,
            email: {
                to: "to@gmail.com",
                from: "from@gmail.com",
                message: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut facilisis sem at felis elementum placerat. Nunc dapibus iaculis luctus. Aenean et volutpat augue. Aenean vehicula ultrices orci ut porta. Praesent metus dui, dapibus vestibulum aliquet et, interdum ut eros. Donec volutpat leo diam, vitae elementum magna ullamcorper at. Suspendisse ut molestie urna. Etiam bibendum vel leo nec elementum. Fusce ut justo in nibh vehicula euismod. Ut cursus finibus mi, at ultricies augue porttitor nec. Aenean in mi eu turpis porttitor dignissim eu eu lorem. Vivamus vitae maximus libero. Quisque scelerisque tincidunt tempus. Quisque sollicitudin pharetra consectetur. Mauris a sapien dapibus, bibendum arcu eu, malesuada libero.`
            }
        }
    ]));
};

describe("Get Emails test", function () {
    it("should not be null", function () {
        expect(get_emails).to.not.be.null;
    });

    it("should be a function", function () {
        expect(get_emails).to.be.a('function');
    })

    it("should return error if parameters aren't informed", () => {

        expect(get_emails()).to.eventually.be.rejectedWith('Code : [90] You should pass a user id as a parameter');
        expect(get_emails(1, null, mock_function)).to.eventually.be.rejectedWith('Code : [90] You should pass a valid connection as parameter');
        expect(get_emails(1, mock_obj)).to.eventually.be.rejectedWith('Code : [90] You should pass a database function');

    });

    it("should return a object with data and response code", () => {
        get_emails(1, mock_obj, mock_function)
            .then((res) => {
                expect(res).to.have.property('code');
                expect(res).to.have.property('emails');
                expect(res.emails).to.be.a('array');
            })
            .catch((err) => err);
    });
});